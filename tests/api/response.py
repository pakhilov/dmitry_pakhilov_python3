import allure
import logging


class AssertableResponse(object):

    def __init__(self, response):
        self._response = response
        logging.info("Request %s", response.request.body)
        logging.info("Response %s", response.json())

    @allure.step("then api response should have {1}")
    def should_have(self, condition):
        condition.match(self._response)
        return self


    @allure.step("get body field {1}")
    def get_field(self, name):
        return self._response.json()[name]

