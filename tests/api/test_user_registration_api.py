from hamcrest import has_length, greater_than, equal_to

from src.api.services import UserApiService
from tests.api.conditions import status_code, body


def test_user_can_register_with_valid_credentials(faker):
    #given
    user = {"username": faker.name(), "password": faker.password(), "email": faker.email()}

    #when
    resp = UserApiService().create_user(user)

    #then
    resp.should_have(status_code(200))\
        .should_have(body("$.id", has_length(greater_than(0))))



def test_can_get_user_by_id(faker):
    #given
    user = {"username": faker.name(), "password": "12345", "email": "deom@gmail.com"}

    resp = UserApiService().create_user(user)

    #when
    resp = UserApiService().get_user_by_id(resp.get_field('id'))

    #then
    resp.should_have(status_code(200))
    resp.should_have(body('password', equal_to("12345")))
