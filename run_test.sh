#!/usr/bin/env bash


rm -rf allure_results/

pytest tests/api -v --alluredir=allure_results

