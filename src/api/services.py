import json
import allure
import requests
import os

from tests.api.response import AssertableResponse


class ApiService(object):

    def __init__(self):
        self._headers = {'content-type': 'application/json'}
        self._base_url = "http://206.189.6.169"  #environ dosen't callable

class UserApiService(ApiService):

    def __init__(self):
        super().__init__()
    @allure.step
    def create_user(self, user):
        _payload = json.dumps(user)

        return AssertableResponse (requests.post(url=self._base_url + "/register",
                             data=_payload,
                             headers=self._headers))
    @allure.step
    def get_user_by_id(self, user_id):
        return AssertableResponse (requests.get(url=self._base_url +"/customers/{id}".format(id=user_id)))
